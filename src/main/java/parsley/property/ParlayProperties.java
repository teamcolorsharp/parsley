package parsley.property;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * Created by Adrian Perez on 5/30/16.
 */
@Component
@ConfigurationProperties(prefix = "parlay")
public class ParlayProperties {

    private String uri;
    private String sendSmsEndpoint;

    public String getUri() {
        return uri;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }

    public String getSendSmsEndpoint() {
        return sendSmsEndpoint;
    }

    public void setSendSmsEndpoint(String sendSmsEndpoint) {
        this.sendSmsEndpoint = sendSmsEndpoint;
    }
}
