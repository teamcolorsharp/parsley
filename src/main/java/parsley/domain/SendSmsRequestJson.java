package parsley.domain;


import com.fasterxml.jackson.annotation.JsonProperty;
import org.parlayx.types.SendSms;

import java.io.Serializable;

/**
 * Created by Adrian Perez on 5/31/16.
 */
public class SendSmsRequestJson implements Serializable {

    private static final long serialVersionUID = 1L;

    //TODO: add validators
    @JsonProperty(value = "charging_information")
    private ChargingInformationJson chargingInformationJson;

    @JsonProperty(value = "simple_reference")
    private SimpleReferenceJson simpleReferenceJson;

    @JsonProperty(value = "message")
    private String message;

    @JsonProperty(value = "sender_name")
    private String senderName;


    public ChargingInformationJson getChargingInformationJson() {
        return chargingInformationJson;
    }

    public void setChargingInformationJson(ChargingInformationJson chargingInformationJson) {
        this.chargingInformationJson = chargingInformationJson;
    }

    public SimpleReferenceJson getSimpleReferenceJson() {
        return simpleReferenceJson;
    }

    public void setSimpleReferenceJson(SimpleReferenceJson simpleReferenceJson) {
        this.simpleReferenceJson = simpleReferenceJson;
    }

    public String getSenderName() {
        return senderName;
    }

    public void setSenderName(String senderName) {
        this.senderName = senderName;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
