package parsley.domain;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

/**
 * Created by Adrian Perez on 5/31/16.
 */
public class ChargingInformationJson implements Serializable {

    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "currency")
    private String currency;

    @JsonProperty(value = "description")
    private String description;

    @JsonProperty(value = "amount")
    private String amount;

    @JsonProperty(value = "code")
    private String code;

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
