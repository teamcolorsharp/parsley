package parsley.domain;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

/**
 * Created by Adrian Perez on 5/31/16.
 */
public class SendSmsResponseJson implements Serializable{


    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "result")
    private String result;

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }
}
