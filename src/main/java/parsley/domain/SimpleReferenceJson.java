package parsley.domain;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.parlayx.types.SimpleReference;

import java.io.Serializable;

/**
 * Created by Adrian Perez on 5/31/16.
 */
public class SimpleReferenceJson implements Serializable {

    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "endpoint")
    private String endpoint;

    @JsonProperty(value = "correlator")
    private String correlator;

    @JsonProperty(value = "interface_name")
    private String interfaceName;

    public String getEndpoint() {
        return endpoint;
    }

    public void setEndpoint(String endpoint) {
        this.endpoint = endpoint;
    }

    public String getCorrelator() {
        return correlator;
    }

    public void setCorrelator(String correlator) {
        this.correlator = correlator;
    }

    public String getInterfaceName() {
        return interfaceName;
    }

    public void setInterfaceName(String interfaceName) {
        this.interfaceName = interfaceName;
    }
}
