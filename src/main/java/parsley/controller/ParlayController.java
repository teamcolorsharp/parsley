package parsley.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import parsley.domain.SendSmsRequestJson;
import parsley.domain.SendSmsResponseJson;
import parsley.service.ParlayClient;

import javax.validation.Valid;


/**
 * Created by Adrian Perez on 5/31/16.
 */
@RestController
@RequestMapping("sms")
public class ParlayController {

    @Autowired
    private ParlayClient parlayClient;

    //TODO: enhance Restfulness
    //TODO: validation
    @RequestMapping(path = "send", method = RequestMethod.POST)
    public SendSmsResponseJson process( @Valid @RequestBody SendSmsRequestJson request) {

        SendSmsResponseJson response = parlayClient.sendSms(request);
        return response;

    }

    @RequestMapping(path = "test", method = RequestMethod.GET)
    public String process() {
        return "(y)";

    }
}
