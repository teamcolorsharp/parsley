package parsley.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;
import parsley.property.ParlayProperties;
import parsley.service.ParlayClient;

/**
 * Created by Adrian Perez on 5/30/16.
 */
@Configuration
public class ParlayConfiguration {

    @Autowired
    private ParlayProperties parlayProperties;

    @Bean
    public Jaxb2Marshaller parlayMarshaller() {
        Jaxb2Marshaller marshaller = new Jaxb2Marshaller();
        marshaller.setContextPath("org.parlayx.types");
        return marshaller;
    }

    @Bean
    public ParlayClient parlayClient(Jaxb2Marshaller parlayMarshaller) {
        ParlayClient client = new ParlayClient(parlayProperties);
        client.setDefaultUri(parlayProperties.getUri());
        client.setMarshaller(parlayMarshaller);
        client.setUnmarshaller(parlayMarshaller);
        return client;
    }
}
