package parsley.service;

import org.parlayx.types.ChargingInformation;
import org.parlayx.types.SendSms;
import org.parlayx.types.SendSmsResponse;
import org.parlayx.types.SimpleReference;
import org.springframework.ws.client.core.support.WebServiceGatewaySupport;
import org.springframework.ws.soap.client.core.SoapActionCallback;
import parsley.domain.ChargingInformationJson;
import parsley.domain.SendSmsRequestJson;
import parsley.domain.SendSmsResponseJson;
import parsley.domain.SimpleReferenceJson;
import parsley.property.ParlayProperties;

import java.math.BigDecimal;
import java.text.DecimalFormat;

/**
 * Created by Adrian Perez on 5/30/16.
 */
public class ParlayClient extends WebServiceGatewaySupport {

    private ParlayProperties parlayProperties;

    public ParlayClient(ParlayProperties parlayProperties) {
        this.parlayProperties = parlayProperties;
    }

    //TODO: Soap Callbacks
    public SendSmsResponseJson sendSms(SendSmsRequestJson sendSmsJsonRequest) {

        SendSms sendSmsRequest = sendSmsMapper(sendSmsJsonRequest);

        SendSmsResponse response = (SendSmsResponse) getWebServiceTemplate().marshalSendAndReceive(
                parlayProperties.getUri(),
                sendSmsRequest,
                new SoapActionCallback(parlayProperties.getSendSmsEndpoint()));

        return sendSmsResponseMapper(response);
    }

    public static SendSmsRequestJson sendSmsMapper(SendSms sendSms){
        SendSmsRequestJson sendSmsRequestJson = new SendSmsRequestJson();
        sendSmsRequestJson.setChargingInformationJson(chargingInformationMapper(sendSms.getCharging()));
        sendSmsRequestJson.setMessage(sendSms.getMessage());
        sendSmsRequestJson.setSenderName(sendSms.getSenderName());
        sendSmsRequestJson.setSimpleReferenceJson(simpleReferenceMapper(sendSms.getReceiptRequest()));
        return sendSmsRequestJson;
    }

    public static SendSms sendSmsMapper(SendSmsRequestJson sendSmsRequestJson){
        SendSms sendSms = new SendSms();
        sendSms.setCharging(chargingInformationMapper(sendSmsRequestJson.getChargingInformationJson()));
        sendSms.setMessage(sendSmsRequestJson.getMessage());
        sendSms.setSenderName(sendSmsRequestJson.getMessage());
        sendSms.setReceiptRequest(simpleReferenceMapper(sendSmsRequestJson.getSimpleReferenceJson()));
        return sendSms;
    }

    public static ChargingInformation chargingInformationMapper(ChargingInformationJson chargingInformationJson){
        ChargingInformation chargingInformation = new ChargingInformation();
        //TODO: enhance string to big decimal parsing
        chargingInformation.setAmount(new BigDecimal(chargingInformationJson.getAmount()));
        chargingInformation.setCode(chargingInformationJson.getCode());
        chargingInformation.setCurrency(chargingInformationJson.getCurrency());
        //chargingInformation.setDescription(chargingInformationJson.getCurrency());
        return chargingInformation;
    }

    public static ChargingInformationJson chargingInformationMapper(ChargingInformation chargingInformation){
        ChargingInformationJson chargingInformationJson = new ChargingInformationJson();
        chargingInformationJson.setAmount(chargingInformation.getAmount().toPlainString());
        chargingInformationJson.setCode(chargingInformation.getCode());
        chargingInformationJson.setCurrency(chargingInformation.getCurrency());
        //chargingInformationJson.setDescription(chargingInformation.getDescription());
        return chargingInformationJson;
    }

    public static SimpleReference simpleReferenceMapper(SimpleReferenceJson simpleReferenceJson){
        SimpleReference simpleReference = new SimpleReference();
        simpleReference.setCorrelator(simpleReferenceJson.getInterfaceName());
        simpleReference.setEndpoint(simpleReferenceJson.getEndpoint());
        simpleReference.setInterfaceName(simpleReferenceJson.getInterfaceName());
        return simpleReference;
    }

    public static SimpleReferenceJson simpleReferenceMapper(SimpleReference simpleReference){
        SimpleReferenceJson simpleReferenceJson = new SimpleReferenceJson();
        simpleReferenceJson.setCorrelator(simpleReference.getCorrelator());
        simpleReferenceJson.setEndpoint(simpleReference.getEndpoint());
        simpleReferenceJson.setInterfaceName(simpleReference.getInterfaceName());
        return simpleReferenceJson;
    }

    public static SendSmsResponse sendSmsResponseMapper(SendSmsResponseJson sendSmsResponseJson){
        SendSmsResponse sendSmsResponse = new SendSmsResponse();
        sendSmsResponse.setResult(sendSmsResponseJson.getResult());
        return sendSmsResponse;
    }

    public static SendSmsResponseJson sendSmsResponseMapper(SendSmsResponse sendSmsResponse){
        SendSmsResponseJson sendSmsResponseJson = new SendSmsResponseJson();
        sendSmsResponseJson.setResult(sendSmsResponse.getResult());
        return sendSmsResponseJson;
    }


}
